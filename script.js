var num1;
var num2;
var operacion;
var cont;

function comenzar(){
    var pant = document.getElementById('pant');
    var reset = document.getElementById('reset')
    var dividir = document.getElementById('division');
    var multiplicar = document.getElementById('multiplicacion');
    var restar = document.getElementById('resta');
    var sumar = document.getElementById('suma');
    var igual = document.getElementById('igual');
    var uno = document.getElementById('uno');
    var dos = document.getElementById('dos');
    var tres = document.getElementById('tres');
    var cuatro = document.getElementById('cuatro');
    var cinco = document.getElementById('cinco');
    var seis = document.getElementById('seis');
    var siete = document.getElementById('siete');
    var ocho = document.getElementById('ocho');
    var nueve = document.getElementById('nueve');
    var cero = document.getElementById('bcero');
    var punto = document.getElementById('punto');

    cont = 0;
    uno.onclick = function(e){
      cont++;
      if (cont==1){
        pant.textContent = pant.textContent = "";
        pant.textContent = pant.textContent  + "1";
      } else {
        pant.textContent = pant.textContent  + "1";
      } 
    }
    cont = 0;
    dos.onclick = function(e){
      cont++;
      if (cont==1){
        pant.textContent = pant.textContent = "";
        pant.textContent = pant.textContent  + "2";
      } else {
        pant.textContent = pant.textContent  + "2";
      }
    } 
    cont = 0;
    tres.onclick = function(e){
      cont++;
      if (cont==1){
        pant.textContent = pant.textContent = "";
        pant.textContent = pant.textContent  + "3";
      } else {
        pant.textContent = pant.textContent  + "3";
      }
    } 
    cont = 0;
    cuatro.onclick = function(e){
      cont++;
      if (cont==1){
        pant.textContent = pant.textContent = "";
        pant.textContent = pant.textContent  + "4";
      } else {
        pant.textContent = pant.textContent  + "4";
      }
    } 
    cont = 0;
    cinco.onclick = function(e){
      cont++;
      if (cont==1){
        pant.textContent = pant.textContent = "";
        pant.textContent = pant.textContent  + "5";
      } else {
        pant.textContent = pant.textContent  + "5";
      }
    } 
    cont = 0;
    seis.onclick = function(e){
      cont++;
      if (cont==1){
        pant.textContent = pant.textContent = "";
        pant.textContent = pant.textContent  + "6";
      } else {
        pant.textContent = pant.textContent  + "6";
      }
    } 
    cont = 0;
    siete.onclick = function(e){
      cont++;
      if (cont==1){
        pant.textContent = pant.textContent = "";
        pant.textContent = pant.textContent  + "7";
      } else {
        pant.textContent = pant.textContent  + "7";
    }
  }
  cont = 0;
    ocho.onclick = function(e){
      cont++;
      if (cont==1){
        pant.textContent = pant.textContent = "";
        pant.textContent = pant.textContent  + "8";
      } else {
        pant.textContent = pant.textContent  + "8";
    } 
  }
  cont = 0;
    nueve.onclick = function(e){
      cont++;
      if (cont==1){
        pant.textContent = pant.textContent = "";
        pant.textContent = pant.textContent  + "9";
      } else {
        pant.textContent = pant.textContent  + "9";
    } 
  }
  cont = 0;
    cero.onclick = function(e){
      cont++;
      if (cont==1){
        pant.textContent = pant.textContent = "";
        pant.textContent = pant.textContent  + "0";
      } else {
        pant.textContent = pant.textContent  + "0";
      }
    } 
    cont = 0;
    punto.onclick = function(e){
      cont++;
      if (cont==1){
        pant.textContent = pant.textContent = "";
        pant.textContent = pant.textContent  + "0.";
      } else {
        pant.textContent = pant.textContent  + '.';
      }
    }
    cont = 0;
    reset.onclick = function(e){
        resetear();
    }

    sumar.onclick = function(e){
        num1=pant.textContent;
        operacion = "+";
        limpiar();
        
    }
    restar.onclick = function(e){
        num1=pant.textContent;
        operacion = "-";
        limpiar();
    }
    multiplicar.onclick = function(e){
        num1=pant.textContent;
        operacion = "X";
        limpiar(); 
    }
    dividir.onclick = function(e){
        num1=pant.textContent;
        operacion = "/";
        limpiar(); 
    }
    igual.onclick = function(e){
        num2=pant.textContent;
        resolver();
        cont = 0;
    }

    function limpiar(){
        pant.textContent= " ";
    }
    
    function resetear (){
        pant.textContent = " ";
        num1 = 0;
        num2 = 0;
        operacion = " ";
    }
}

function resolver(){
    var res = 0;
    switch(operacion){
        case "+":
            res = parseFloat(num1) + parseFloat(num2);
            break;
        case "-":
            res = parseFloat(num1) - parseFloat(num2);
            break;
        case "X":
            res = parseFloat(num1) * parseFloat(num2);
            break;
        case "/":
            res = parseFloat(num1) / parseFloat(num2);
            break;
    }
    pant.textContent = res;
}
